import requests as rq
import random
DEBUG = False

class MMBot:
    words = [word.strip() for word in open("/home/xmwriam/wordle/words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)
        self.choices = self.words 
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            right = rj["feedback"]
            if 'exceeded' in rj['message']:
                return right, rj['answer']
            return right, False

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, status = post(choice)
        tries = [f'{choice}:{right}']

        for _ in range(5):
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = self.choices[0]
            self.choices.remove(choice)
            right, status = post(choice)
            if status:
                tries.append(f'{choice}:{right}')
                print("Failed, answer was", status )
                print("Route was:", " => ".join(tries))
                break
            tries.append(f'{choice}:{right}')
            if right == 'GGGGG':
                print("Answer is", choice, "found in", len(tries), "attempts")
                print("Route is:", " => ".join(tries))
                break

    def update(self, choice: str, right: int):
        for i in range(len(right)):
            if right[i] == 'G':
                self.choices = [w for w in self.choices if w[i] == choice[i]]
            elif right[i] == 'Y':
                self.choices = [w for w in self.choices if (choice[i] in w and choice[i] != w[i])]
            else:
                self.choices = [w for w in self.choices if choice[i] not in w]


game = MMBot("Mariam")
game.play()
