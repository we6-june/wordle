
# wordle
This is a group project on WORDLE GAME:
PROBLEM STATEMENT - Create a chatbot that guesses a five letter word continuously within 6 attempts until the correct word is guessed.

#Input
Input - five letter string. 

#Output
Output - 'G' if the guessed letter is correct and is in its right position, 
'Y' if the guessed letter is correct but not in its right position, 
'R' if the guessed letter is incorrect. 

#Team
Team members - 
Mariam Eqbal, 
Mani Harshitha, 
Amolly Choudhary, 
Varshini Reddy. 
