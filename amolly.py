import requests as rq
import random
DEBUG = False

class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        self.choices = [w for w in MMBot.words[:] if is_unique(w)]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            right = rj["feedback"]
            status = "win" in rj["message"]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice}:{right}']

        for _ in range(6):
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
        if len(tries) > 6:
            print("Failed in finding the secret")
        elif right == "GGGGG":
            print("Secret is", choice, "found in", len(tries), "attempts")
            print("Route is:", " => ".join(tries))

    def update(self, choice: str, right: int):
        new_choices = []
        for word in self.choices:
            match = True
            for i, (c, f) in enumerate(zip(choice, right)):
                if f == "G" and word[i] != c:
                    match = False
                    break
                if f == "Y" and (c not in word or word[i] == c):
                    match = False
                    break
                if f == "R" and c in word:
                    match = False
                    break
            if match:
                new_choices.append(word)
        self.choices = new_choices


game = MMBot("Amolly")
game.play()

